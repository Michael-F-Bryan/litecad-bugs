﻿using System;

namespace LitecadBugs
{
    public static class LitecadEvents
    {
        private static readonly F_LCEVENT EventHandler;

        public static event EventHandler<LitecadEventArgs> EventRaised;
        /// <summary>
        /// An error was thrown by an event handler.
        /// </summary>
        public static event EventHandler<LitecadEventHandlerErrorEventArgs> EventHandlerError;

        static LitecadEvents()
        {
            EventHandler = OnLitecadEvent;

            int[] events = new int[]{
                Lcad.LC_EVENT_LBDOWN,
                Lcad.LC_EVENT_LBDBLCLK,
                Lcad.LC_EVENT_RBDOWN,
                Lcad.LC_EVENT_RBUP,
            };

            foreach (int ev in events)
            {
                Lcad.EventSetProc(ev, EventHandler, 0, IntPtr.Zero);
            }
        }

        private static void OnLitecadEvent(IntPtr hEvent)
        {
            var args = new LitecadEventArgs(hEvent);
            try
            {
                EventRaised?.Invoke(null, args);
            }
            catch (Exception ex)
            {
                OnEventHandlerError(args, ex);
            }
            finally
            {
                Lcad.EventReturnCode(args.ReturnCode);
            }
        }

        private static void OnEventHandlerError(LitecadEventArgs args, Exception ex)
        {
            try
            {
                EventHandlerError?.Invoke(null, new LitecadEventHandlerErrorEventArgs(ex, args));
            }
            catch
            {
                // ignore errors so we don't unwind across the FFI boundary                
            }
        }
    }

    public class LitecadEventHandlerErrorEventArgs : EventArgs
    {
        public LitecadEventHandlerErrorEventArgs(Exception ex, LitecadEventArgs args)
        {
            Error = ex;
            OriginalEventArgs = args;
        }

        public Exception Error { get; }
        public LitecadEventArgs OriginalEventArgs { get; }
    }

    public class LitecadEventArgs : EventArgs
    {
        internal LitecadEventArgs(IntPtr handle)
        {
            Handle = handle;
        }

        public IntPtr Handle { get; }
        public int ReturnCode { get; set; } = 0;

        public int EventType => Lcad.PropGetInt(Handle, Lcad.LC_PROP_EVENT_TYPE);
        public IntPtr Window => Lcad.PropGetHandle(Handle, Lcad.LC_PROP_EVENT_WND);
        public IntPtr Drawing => Lcad.PropGetHandle(Handle, Lcad.LC_PROP_EVENT_DRW);
        public IntPtr Block => Lcad.PropGetHandle(Handle, Lcad.LC_PROP_EVENT_BLOCK);
        public IntPtr Entity => Lcad.PropGetHandle(Handle, Lcad.LC_PROP_EVENT_ENTITY);

        public int IntegerValue(int ix)
        {
            switch (ix)
            {
                case 1:
                    return Lcad.PropGetInt(Handle, Lcad.LC_PROP_EVENT_INT1);
                case 2:
                    return Lcad.PropGetInt(Handle, Lcad.LC_PROP_EVENT_INT2);
                case 3:
                    return Lcad.PropGetInt(Handle, Lcad.LC_PROP_EVENT_INT3);
                case 4:
                    return Lcad.PropGetInt(Handle, Lcad.LC_PROP_EVENT_INT4);
                case 5:
                    return Lcad.PropGetInt(Handle, Lcad.LC_PROP_EVENT_INT5);

                default:
                    throw new ArgumentOutOfRangeException(nameof(ix), ix, "Invalid integer property");
            }
        }

        public string StringValue(int ix)
        {
            switch (ix)
            {
                case 1:
                    return Lcad.PropGetStr(Handle, Lcad.LC_PROP_EVENT_STR1);
                case 2:
                    return Lcad.PropGetStr(Handle, Lcad.LC_PROP_EVENT_STR2);

                default:
                    throw new ArgumentOutOfRangeException(nameof(ix), ix, "Invalid string property");
            }
        }
    }
}
