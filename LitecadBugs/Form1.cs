using System;
using System.Windows.Forms;

namespace LitecadBugs
{
    public partial class Form1 : Form
    {
        IntPtr DrawingDatabase = IntPtr.Zero;
        IntPtr GraphicsWindow = IntPtr.Zero;
        IntPtr Block = IntPtr.Zero;
        IntPtr PropertiesWindow = IntPtr.Zero;

        public Form1()
        {
            InitializeComponent();
        }

        private void LitecadEvents_EventRaised(object sender, LitecadEventArgs e)
        {
            switch (e.EventType)
            {
                case Lcad.LC_EVENT_RBDOWN:
                    ContextMenuStrip?.Show(this, PointToClient(Cursor.Position));
                    // Prevent the default context menu
                    e.ReturnCode = 1;
                    break;

                default:
                    return;
            }
        }

        private void OnDrawPointClicked(object sender, EventArgs e)
        {
            Lcad.WndExeCommand(GraphicsWindow, Lcad.LC_CMD_POINT, Lcad.LC_ARC_SME);
        }

        private void OnDrawLineClicked(object sender, EventArgs e)
        {
            Lcad.WndExeCommand(GraphicsWindow, Lcad.LC_CMD_LINE, 0);
        }

        private void OnDrawArcClicked(object sender, EventArgs e)
        {
            Lcad.WndExeCommand(GraphicsWindow, Lcad.LC_CMD_ARC, 0);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Do any final LiteCAD cleanup before exiting.

            if (PropertiesWindow != IntPtr.Zero)
            {
                Lcad.DeleteProps(PropertiesWindow);
            }
            if (GraphicsWindow != IntPtr.Zero)
            {
                Lcad.DeleteWindow(GraphicsWindow);
            }
            if (DrawingDatabase != IntPtr.Zero)
            {
                Lcad.DeleteDrawing(DrawingDatabase);
            }

            UnregisterEventHandles();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RegisterEventHandlers();
            PopulateContextMenu();

            GraphicsWindow = Lcad.CreateWindow(panel1.Handle, 0);
            PropertiesWindow = Lcad.CreateProps(panel2.Handle);
            DrawingDatabase = Lcad.CreateDrawing();
            Block = Lcad.PropGetHandle(DrawingDatabase, Lcad.LC_PROP_DRW_BLOCK);

            Lcad.WndSetBlock(GraphicsWindow, Block);
            Lcad.WndSetProps(GraphicsWindow, PropertiesWindow);

            // make sure the canvas is the correct size on startup
            OnResize(EventArgs.Empty);
        }

        private void PopulateContextMenu()
        {
            contextMenuStrip1.Items.Add(new ToolStripMenuItem("Draw Point", null, OnDrawPointClicked));
            contextMenuStrip1.Items.Add(new ToolStripMenuItem("Draw Line", null, OnDrawLineClicked));
            contextMenuStrip1.Items.Add(new ToolStripMenuItem("Draw Arc", null, OnDrawArcClicked));
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Redraw();
        }

        private void Redraw()
        {
            if (Block != IntPtr.Zero)
            {
                Lcad.BlockUpdate(Block, true, IntPtr.Zero);
            }

            if (GraphicsWindow != IntPtr.Zero)
            {
                // Make sure the LiteCAD window re-paints as well
                Lcad.WndRedraw(GraphicsWindow);
            }
        }

        private void RegisterEventHandlers()
        {
            LitecadEvents.EventRaised += LitecadEvents_EventRaised;
        }

        private void UnregisterEventHandles()
        {
            LitecadEvents.EventRaised -= LitecadEvents_EventRaised;
        }

        private void ResizeLitecadComponents()
        {
            if (GraphicsWindow != IntPtr.Zero)
            {
                Lcad.WndResize(GraphicsWindow, 0, 0, panel1.Width, panel1.Height);
            }
            if (PropertiesWindow != IntPtr.Zero)
            {
                Lcad.PropsResize(PropertiesWindow, 0, 0, panel2.Width, panel2.Height);
            }
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            ResizeLitecadComponents();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            ResizeLitecadComponents();
        }
    }
}
