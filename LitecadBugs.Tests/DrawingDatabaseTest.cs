﻿using System;
using Xunit;

namespace LitecadBugs.Tests
{
    public class DrawingDatabaseTest
    {
        [Fact]
        public void CreateADrawingDatabase()
        {
            var handle = Lcad.CreateDrawing();

            Assert.NotEqual(IntPtr.Zero, handle);

            Lcad.DeleteDrawing(handle);
        }
    }
}
